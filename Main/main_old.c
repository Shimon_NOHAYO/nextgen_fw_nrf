/* Copyright (c) 2012 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

/** @file
 *
 * @defgroup ble_sdk_uart_over_ble_main main.c
 * @{
 * @ingroup  ble_sdk_app_nus_eval
 * @brief    UART over BLE application main file.
 *
 * This file contains the source code for a sample application that uses the Nordic UART service.
 * This application uses the @ref srvlib_conn_params module.
 */

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "nordic_common.h"
#include "nrf.h"
#include "nrf51_bitfields.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_conn_params.h"
#include "softdevice_handler.h"
#include "app_timer.h"
#include "app_button.h"
#include "ble_nus.h"
#include "simple_uart.h"
#include "boards.h"
#include "ble_error_log.h"
#include "app_uart.h"
#include "nrf_delay.h"
#include "ble_debug_assert_handler.h"
//#include "ant_stack_handler_types.h"
//#include "ant_interface.h"
//#include "ant_parameters.h"


#define DEVICE_NAME                     "MsS_FB_"  /**< Name of device. Will be included in the advertising data. */

#define RX_PIN_NUMBER  									0   	//10 UART RX pin number.
#define TX_PIN_NUMBER  									2   	// 9 UART TX pin number. 
#define RF_CON													8			// Charge pin number.
                                              
#define APP_ADV_INTERVAL                64    /**< The advertising interval (in units of 0.625 ms. This value corresponds to 40 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS      180   /**< The advertising timeout (in units of seconds). */
                                              
#define APP_TIMER_PRESCALER             0     /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_MAX_TIMERS            2     /**< Maximum number of simultaneously created timers. */
#define APP_TIMER_OP_QUEUE_SIZE         4     /**< Size of timer operation queues. */
                                              
#define MIN_CONN_INTERVAL               16    /**< Minimum acceptable connection interval (20 ms), Connection interval uses 1.25 ms units. */
#define MAX_CONN_INTERVAL               60    /**< Maximum acceptable connection interval (75 ms), Connection interval uses 1.25 ms units. */
#define SLAVE_LATENCY                   0     /**< slave latency. */
#define CONN_SUP_TIMEOUT                400   /**< Connection supervisory timeout (4 seconds), Supervision Timeout uses 10 ms units. */
#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(20000, APP_TIMER_PRESCALER) /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (20 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(5000, APP_TIMER_PRESCALER)  /**< Time between each call to sd_ble_gap_conn_param_update after the first call (5 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                           /**< Number of attempts before giving up the connection parameter negotiation. */

#define BUTTON_DETECTION_DELAY          APP_TIMER_TICKS(50, APP_TIMER_PRESCALER)    /**< Delay from a GPIOTE event until a button is reported as pushed (in number of timer ticks). */

#define SEC_PARAM_TIMEOUT               30       /**< Timeout for Pairing Request or Security Request (in seconds). */
#define SEC_PARAM_BOND                  1         /**< Perform bonding. */
#define SEC_PARAM_MITM                  0         /**< Man In The Middle protection not required. */
#define SEC_PARAM_IO_CAPABILITIES       BLE_GAP_IO_CAPS_NONE    /**< No I/O capabilities. */
#define SEC_PARAM_OOB                   0         /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE          7         /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE          16        /**< Maximum encryption key size. */

#define START_STRING                    "Start...\n"   /**< The string that will be sent over the UART when the application starts. */

#define DEAD_BEEF                       0xDEADBEEF     /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

//UART
#define PKT_HEADER 							0xFA
#define PKT_FOOTER 							0xFB
#define PKT_FOOTER_FW_MSG				0xFC

#define UART_TX_BUF_SIZE 								256u 		 /**< UART Tx buffer size. */
#define UART_RX_BUF_SIZE 								1u   		 /**< UART Rx buffer size. */

#define RFM_VER                         0x11
#define RFM_BLE_CON                     0x33

//#define ANT_HRMRX_ANT_CHANNEL           0      /**< Default ANT Channel. */
//#define ANT_HRMRX_CHANNEL_TYPE          0x40   /**< Channel Type Slave RX only. */
//#define ANT_HRMRX_DEVICE_TYPE           0x78   /**< Channel ID device type. */
//#define ANT_HRMRX_DEVICE_NUMBER         0      /**< Device Number. */
//#define ANT_HRMRX_TRANS_TYPE            0      /**< Transmission Type. */
//#define ANT_HRMRX_MSG_PERIOD            0x1F86 /**< Message Periods, decimal 8070 (4.06Hz). */
//#define ANT_HRMRX_EXT_ASSIGN            0x00   /**< ANT Ext Assign. */
//#define ANT_HRM_TOGGLE_MASK             0x80   /**< HRM Page Toggle Bit Mask. */
//#define ANTPLUS_NETWORK_NUMBER          0      /**< Network number. */
//#define ANTPLUS_RF_FREQ                 0x39   /**< Frequency, Decimal 57 (2457 MHz). */
//#define ANT_HRM_PAGE_0                  0      /**< HRM page 0 constant. */
//#define ANT_HRM_PAGE_1                  1      /**< HRM page 1 constant. */
//#define ANT_HRM_PAGE_2                  2      /**< HRM page 2 constant. */
//#define ANT_HRM_PAGE_3                  3      /**< HRM page 3 constant. */
//#define ANT_HRM_PAGE_4                  4      /**< HRM page 4 constant. */
//#define ANT_BUFFER_INDEX_MESG_ID        0x01   /**< Index for Message ID. */
//#define ANT_BUFFER_INDEX_MESG_DATA      0x03   /**< Index for Data. */

//#define ANT_HRMRX_NETWORK_KEY           {0xB9, 0xA5, 0x21, 0xFB, 0xBD, 0x72, 0xC3, 0x45} ///< The default network key used. 



//static uint8_t                          m_ant_network_key[] = ANT_HRMRX_NETWORK_KEY; /**< ANT PLUS network key. */

static ble_gap_sec_params_t             m_sec_params;                               /**< Security requirements for this application. */
static uint16_t                         m_conn_handle = BLE_CONN_HANDLE_INVALID;    /**< Handle of the current connection. */
static ble_nus_t                        m_nus;                                      /**< Structure to identify the Nordic UART Service. */
uint8_t data_array1[BLE_NUS_MAX_DATA_LEN] = {'H','R',':','9','8',' ','C','a','l',':','8','5',' ','S','t','e','p',':','9','0'};
int16_t AccX = 0;
int16_t AccY = 0;
int16_t AccZ = 0;
uint8_t         computed_heart_rate=50;
uint32_t pin_rf_con_state=1;

/**@brief     Error handler function, which is called when an error has occurred.
 *
 * @warning   This handler is an example only and does not fit a final product. You need to analyze
 *            how your product is supposed to react in case of error.
 *
 * @param[in] error_code  Error code supplied to the handler.
 * @param[in] line_num    Line number where the handler is called.
 * @param[in] p_file_name Pointer to the file name. 
 */
void app_error_handler(uint32_t error_code, uint32_t line_num, const uint8_t * p_file_name)
{
    // This call can be used for debug purposes during application development.
    // @note CAUTION: Activating this code will write the stack to flash on an error.
    //                This function should NOT be used in a final product.
    //                It is intended STRICTLY for development/debugging purposes.
    //                The flash write will happen EVEN if the radio is active, thus interrupting
    //                any communication.
    //                Use with care. Un-comment the line below to use.
    // ble_debug_assert_handler(error_code, line_num, p_file_name);

    // On assert, the system can only recover with a reset.
    NVIC_SystemReset();
}


/**@brief       Assert macro callback function.
 *
 * @details     This function will be called in case of an assert in the SoftDevice.
 *
 * @warning     This handler is an example only and does not fit a final product. You need to
 *              analyze how your product is supposed to react in case of Assert.
 * @warning     On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in]   line_num   Line number of the failing ASSERT call.
 * @param[in]   file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}


/**@brief   Function for Timer initialization.
 *
 * @details Initializes the timer module.
 */
static void timers_init(void)
{
    // Initialize timer module
    APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_MAX_TIMERS, APP_TIMER_OP_QUEUE_SIZE, false);
}


/**@brief   Function for the GAP initialization.
 *
 * @details This function will setup all the necessary GAP (Generic Access Profile)
 *          parameters of the device. It also sets the permissions and appearance.
 */
static void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);
    
    //err_code = sd_ble_gap_device_name_set(&sec_mode,(const uint8_t *) DEVICE_NAME,strlen(DEVICE_NAME));
    uint8_t name[strlen(DEVICE_NAME) + 2];
		
	  sprintf((char *) name, "%s%x", DEVICE_NAME, (uint8_t) NRF_FICR->DEVICEID[0]);
	
     err_code = sd_ble_gap_device_name_set(&sec_mode,(const uint8_t *)name,2+strlen(DEVICE_NAME));
                                      
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}


/**@brief   Function for the Advertising functionality initialization.
 *
 * @details Encodes the required advertising data and passes it to the stack.
 *          Also builds a structure to be passed to the stack when starting advertising.
 */
static void advertising_init(void)
{
    uint32_t      err_code;
    ble_advdata_t advdata;
    ble_advdata_t scanrsp;
    uint8_t       flags = BLE_GAP_ADV_FLAGS_LE_ONLY_LIMITED_DISC_MODE;
    
    ble_uuid_t adv_uuids[] = {{BLE_UUID_NUS_SERVICE, m_nus.uuid_type}};

    memset(&advdata, 0, sizeof(advdata));
    advdata.name_type               = BLE_ADVDATA_FULL_NAME;
    advdata.include_appearance      = false;
    advdata.flags.size              = sizeof(flags);
    advdata.flags.p_data            = &flags;

    memset(&scanrsp, 0, sizeof(scanrsp));
    scanrsp.uuids_complete.uuid_cnt = sizeof(adv_uuids) / sizeof(adv_uuids[0]);
    scanrsp.uuids_complete.p_uuids  = adv_uuids;
    
    err_code = ble_advdata_set(&advdata, &scanrsp);
    APP_ERROR_CHECK(err_code);
}


/**@brief    Function for handling the data from the Nordic UART Service.
 *
 * @details  This function will process the data received from the Nordic UART BLE Service and send
 *           it to the UART module.
 */
/**@snippet [Handling the data received over BLE] */
void nus_data_handler(ble_nus_t * p_nus, uint8_t * p_data, uint16_t length)
{
    //for (int i = 0; i < length; i++)
    //{
    //    simple_uart_put(p_data[i]);
    //}
		//ble_nus_send_string(&m_nus, p_data, 20); // imidiate readback that order was recived
		app_uart_put((uint8_t)p_data[0]); //send info to ST to process order
//    simple_uart_put('\n');
}
/**@snippet [Handling the data received over BLE] */


/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void)
{
    uint32_t         err_code;
    ble_nus_init_t   nus_init;
    
    memset(&nus_init, 0, sizeof(nus_init));

    nus_init.data_handler = nus_data_handler;
    
    err_code = ble_nus_init(&m_nus, &nus_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing security parameters.
 */
static void sec_params_init(void)
{
    m_sec_params.timeout      = SEC_PARAM_TIMEOUT;
    m_sec_params.bond         = SEC_PARAM_BOND;
    m_sec_params.mitm         = SEC_PARAM_MITM;
    m_sec_params.io_caps      = SEC_PARAM_IO_CAPABILITIES;
    m_sec_params.oob          = SEC_PARAM_OOB;  
    m_sec_params.min_key_size = SEC_PARAM_MIN_KEY_SIZE;
    m_sec_params.max_key_size = SEC_PARAM_MAX_KEY_SIZE;
}


/**@brief       Function for handling an event from the Connection Parameters Module.
 *
 * @details     This function will be called for all events in the Connection Parameters Module
 *              which are passed to the application.
 *
 * @note        All this function does is to disconnect. This could have been done by simply setting
 *              the disconnect_on_fail config parameter, but instead we use the event handler
 *              mechanism to demonstrate its use.
 *
 * @param[in]   p_evt   Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;
    
    if(p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief       Function for handling errors from the Connection Parameters module.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;
    
    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;
    
    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for starting advertising.
 */
static void advertising_start(void)
{
    uint32_t             err_code;
    ble_gap_adv_params_t adv_params;
    
    // Start advertising
    memset(&adv_params, 0, sizeof(adv_params));
    
    adv_params.type        = BLE_GAP_ADV_TYPE_ADV_IND;
    adv_params.p_peer_addr = NULL;
    adv_params.fp          = BLE_GAP_ADV_FP_ANY;
    adv_params.interval    = APP_ADV_INTERVAL;
    adv_params.timeout     = APP_ADV_TIMEOUT_IN_SECONDS;

    err_code = sd_ble_gap_adv_start(&adv_params);
    APP_ERROR_CHECK(err_code);

//    nrf_gpio_pin_set(ADVERTISING_LED_PIN_NO);
}
uint8_t data2xmit_buffer[1400];
uint16_t put_index = 0;
uint16_t take_index = 0;
 uint8_t uart_byte = 0;
	 uint8_t uart_byte_d = 0;
	 uint8_t uart_byte_dd = 0;

static void heart_rate_meas_send(void)
{
    static uint32_t cnt = 0;
    uint32_t        err_code;
    uint16_t        heart_rate;
	
	  static uint8_t old_uart_byte = 0;
	  static bool flag_data_waiting_for_xmit = false;
	  static bool flag_xmit_mode = false;
	  bool no_present_data = false;
	
	//app_uart_put((uint8_t) 0xAA);

    while (true)
    {
        //heart_rate = (uint16_t)ble_sensorsim_measure(&m_heart_rate_sim_state, &m_heart_rate_sim_cfg);
				//if (flag_data_waiting_for_xmit == false) {
				while (app_uart_get(&uart_byte) == NRF_SUCCESS) {
					//if (uart_byte == 0xEE /*&& (uart_byte_d == 0xEE) && (uart_byte_dd == 0xEE)*/) {put_index = 0;}
					//if (uart_byte == 0) {put_index = 0;}
					data2xmit_buffer[put_index++ ] = uart_byte;
					if (put_index >= 20) 
					 {
						take_index = 0; // ignoring byte 0 which is the last 0x55 header 
						flag_xmit_mode = true;
						app_uart_put((uint8_t) 0xBB); //Send Ack to ST
					//		nrf_delay_us(1000);
					//	app_uart_put((uint8_t) 0xBB); //Send Ack to ST
					//	app_uart_put((uint8_t) 0xBB); 
						
						put_index = 0;
						
					}
			//		uart_byte_dd = uart_byte_d;
			//		uart_byte_d = uart_byte;
				} 
		 //else {
      //  if (app_uart_get(&uart_byte) == NRF_ERROR_NOT_FOUND) {
			//		//flag_data_waiting_for_xmit = false;
			//		no_present_data = true;
			//		break;
			//	} else {
					
			//	}
		//	} else {
		//		uart_byte = 255; //old_uart_byte;
		//	}
			  if (flag_xmit_mode == true) {
			  //sprintf((char*) data_array1,"%017d!!!",data2xmit_buffer[take_index]);
		//		sprintf((char*) data_array1,"%02d%02d%02d%02d%02d%02d%02d%02d%02d%02d",data2xmit_buffer[take_index],
		//		data2xmit_buffer[take_index+1],data2xmit_buffer[take_index+2],data2xmit_buffer[take_index+3],data2xmit_buffer[take_index+4],
		//	  data2xmit_buffer[take_index+5],data2xmit_buffer[take_index+6],data2xmit_buffer[take_index+7],data2xmit_buffer[take_index+8],
		//		data2xmit_buffer[take_index+9]);
				
				data_array1[0] = data2xmit_buffer[take_index+0]; 
				data_array1[1] = data2xmit_buffer[take_index+1];
				data_array1[2] = data2xmit_buffer[take_index+2];
				data_array1[3] = data2xmit_buffer[take_index+3];
				data_array1[4] = data2xmit_buffer[take_index+4];
				data_array1[5] = data2xmit_buffer[take_index+5];
				data_array1[6] = data2xmit_buffer[take_index+6];
				data_array1[7] = data2xmit_buffer[take_index+7];
				data_array1[8] = data2xmit_buffer[take_index+8];
				data_array1[9] = data2xmit_buffer[take_index+9];
				data_array1[10] = data2xmit_buffer[take_index+10];
				data_array1[11] = data2xmit_buffer[take_index+11];
				data_array1[12] = data2xmit_buffer[take_index+12];
				data_array1[13] = data2xmit_buffer[take_index+13];
				data_array1[14] = data2xmit_buffer[take_index+14];
				data_array1[15] = data2xmit_buffer[take_index+15];
				data_array1[16] = data2xmit_buffer[take_index+16];
				data_array1[17] = data2xmit_buffer[take_index+17];
				data_array1[18] = data2xmit_buffer[take_index+18];
				data_array1[19] = data2xmit_buffer[take_index+19];
				
				err_code = ble_nus_send_string(&m_nus, data_array1, 20);
				
			//	nrf_delay_us(10);
							
				if (err_code == NRF_SUCCESS) {take_index=take_index+20;}// sending the data in binary and not ascii
				
				/*for (uint8_t k=0; k<10; k++) {
					data_array1[k] = data2xmit_buffer[take_index+k];
				}					
				err_code = ble_nus_send_string(&m_nus, data_array1, 20);
				if (err_code == NRF_SUCCESS) {take_index=take_index+10;}*/
				
				if (take_index >= 20) { // (data2xmit_buffer[take_index] >= 0x64) { //
					flag_xmit_mode = false;
					take_index = 0;
					
			//		for (int l=0; l<0x64; l++) {data2xmit_buffer[l]=9;}
				//	nrf_delay_us(1000);
					app_uart_put((uint8_t) 0xAA);
				}
        //err_code = ble_hrs_heart_rate_measurement_send(&m_hrs, heart_rate);
        if (err_code == BLE_ERROR_NO_TX_BUFFERS ||
            err_code == NRF_ERROR_INVALID_STATE || 
            err_code == BLE_ERROR_GATTS_SYS_ATTR_MISSING)
				{
				//	  flag_data_waiting_for_xmit = true;
				  // 	old_uart_byte = uart_byte;
				//		app_uart_put((uint8_t) 0x05);
            break; // Shimon
        }
        else if (err_code != NRF_SUCCESS) 
        {
            APP_ERROR_HANDLER(err_code);
        }

        // Disable RR Interval recording every third heart rate measurement.
        // NOTE: An application will normally not do this. It is done here just for testing generation
        //       of messages without RR Interval measurements.
        //m_rr_interval_enabled = ((cnt % 3) != 0);
				//flag_data_waiting_for_xmit = false;
			}
			//pin_rf_con_state = nrf_gpio_pin_read(RF_CON);
			//if (pin_rf_con_state == 0) 
			//	NVIC_SystemReset();
    }//While
}

/**@brief       Function for the Application's S110 SoftDevice event handler.
 *
 * @param[in]   p_ble_evt   S110 SoftDevice event.
 */
static void on_ble_evt(ble_evt_t * p_ble_evt)
{
    uint32_t                         err_code;
    static ble_gap_evt_auth_status_t m_auth_status;
    ble_gap_enc_info_t *             p_enc_info;
    
    switch (p_ble_evt->header.evt_id)
    {
			
			  case BLE_EVT_TX_COMPLETE:
            //heart_rate_meas_send(); 
            break;
				
				
        case BLE_GAP_EVT_CONNECTED:
            //nrf_gpio_pin_set(CONNECTED_LED_PIN_NO);
            //nrf_gpio_pin_clear(ADVERTISING_LED_PIN_NO);
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
						app_uart_put((uint8_t) 0xAA); //request data from ST.
						put_index = 0;
						//heart_rate_meas_send();
            break;
            
        case BLE_GAP_EVT_DISCONNECTED:
           // nrf_gpio_pin_clear(CONNECTED_LED_PIN_NO);
            m_conn_handle = BLE_CONN_HANDLE_INVALID;

            advertising_start();

            break;
            
        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            err_code = sd_ble_gap_sec_params_reply(m_conn_handle, 
                                                   BLE_GAP_SEC_STATUS_SUCCESS, 
                                                   &m_sec_params);
            APP_ERROR_CHECK(err_code);
            break;
            
        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
            err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_AUTH_STATUS:
            m_auth_status = p_ble_evt->evt.gap_evt.params.auth_status;
            break;
            
        case BLE_GAP_EVT_SEC_INFO_REQUEST:
            p_enc_info = &m_auth_status.periph_keys.enc_info;
            if (p_enc_info->div == p_ble_evt->evt.gap_evt.params.sec_info_request.div)
            {
                err_code = sd_ble_gap_sec_info_reply(m_conn_handle, p_enc_info, NULL);
                APP_ERROR_CHECK(err_code);
            }
            else
            {
                // No keys found for this device
                err_code = sd_ble_gap_sec_info_reply(m_conn_handle, NULL, NULL);
                APP_ERROR_CHECK(err_code);
            }
            break;

        case BLE_GAP_EVT_TIMEOUT:
            if (p_ble_evt->evt.gap_evt.params.timeout.src == BLE_GAP_TIMEOUT_SRC_ADVERTISEMENT)
            { 
                // Go to system-off mode (this function will not return; wakeup will cause a reset)
                //err_code = sd_power_system_off();    
                APP_ERROR_CHECK(err_code);
            }
            break;

        default:
            // No implementation needed.
            break;
    }
}

/**@brief Handle received ANT data message.
 * 
 * @param[in]  p_evt_buffer   The buffer containg received data. 
 */
//static void ant_data_messages_handle(uint8_t * p_evt_buffer)
//{
//  //uint8_t         computed_heart_rate;
//	
//    computed_heart_rate = (uint8_t)p_evt_buffer[ANT_BUFFER_INDEX_MESG_DATA + 7];
//		//app_uart_put((uint8_t)computed_heart_rate);
//}

/**@brief ANT RX event handler.
 *
 * @param[in]   p_ant_evt   Event received from the stack.
 */
//static void on_ant_evt_rx(ant_evt_t * p_ant_evt)
//{
//    uint32_t message_id = p_ant_evt->evt_buffer[ANT_BUFFER_INDEX_MESG_ID];

//    switch (message_id)
//    {
//        case MESG_BROADCAST_DATA_ID:
//        case MESG_ACKNOWLEDGED_DATA_ID:
//			  case MESG_BURST_DATA_ID: 
//            ant_data_messages_handle(p_ant_evt->evt_buffer);
//            break;

//        default:
//            // No implementation needed.
//            break;
//    }
//}


/**@brief Application's Stack ANT event handler.
 *
 * @param[in]   p_ant_evt   Event received from the stack.
 */
//static void on_ant_evt(ant_evt_t * p_ant_evt)
//{
//    if (p_ant_evt->channel == ANT_HRMRX_ANT_CHANNEL)
//    {
//        switch (p_ant_evt->event)
//        {
//            case EVENT_RX:
//                on_ant_evt_rx(p_ant_evt);
//                break;

//            default:
//                // No implementation needed.
//                break;
//        }
//    }
//}


/**@brief       Function for dispatching a S110 SoftDevice event to all modules with a S110
 *              SoftDevice event handler.
 *
 * @details     This function is called from the S110 SoftDevice event interrupt handler after a
 *              S110 SoftDevice event has been received.
 *
 * @param[in]   p_ble_evt   S110 SoftDevice event.
 */
static void ble_evt_dispatch(ble_evt_t * p_ble_evt)
{
    ble_conn_params_on_ble_evt(p_ble_evt);
    ble_nus_on_ble_evt(&m_nus, p_ble_evt);
    on_ble_evt(p_ble_evt);
}


/**@brief   Function for the S110 SoftDevice initialization.
 *
 * @details This function initializes the S110 SoftDevice and the BLE event interrupt.
 */
static void ble_ant_stack_init(void)
{
    // Initialize SoftDevice.
    SOFTDEVICE_HANDLER_INIT(NRF_CLOCK_LFCLKSRC_XTAL_20_PPM, false); 
    
	//SOFTDEVICE_HANDLER_INIT(NRF_CLOCK_LFCLKSRC_SYNTH_250_PPM/*NRF_CLOCK_LFCLKSRC_RC_250_PPM_2000MS_CALIBRATION*/, false);
    // Subscribe for BLE events.
    uint32_t err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
    APP_ERROR_CHECK(err_code);
	
    // Subscribe for ANT events.
//    err_code = softdevice_ant_evt_handler_set(on_ant_evt);
  //  APP_ERROR_CHECK(err_code);
}


///**@brief  Function for placing the application in low power state while waiting for events.
// */
//static void power_manage(void)
//{
//    uint32_t err_code = sd_app_evt_wait();
//    APP_ERROR_CHECK(err_code);
//}


/**@brief Function for handling an UART error.
 *
 * @param[in] p_event     Event supplied to the handler.
 */
void uart_error_handle(app_uart_evt_t * p_event)
{
    if ((p_event->evt_type == APP_UART_FIFO_ERROR) || 
        (p_event->evt_type == APP_UART_COMMUNICATION_ERROR))
    {
        // Copy parameters to static variables because parameters are not accessible in the 
        // debugger.
        static volatile app_uart_evt_t uart_event;

        uart_event.evt_type = p_event->evt_type;
        uart_event.data     = p_event->data;
        UNUSED_VARIABLE(uart_event);  

    }
}

/**@brief  Function for initializing the UART module.
 */
static void uart_init(void)
{
    //@snippet [UART Initialization] 
		/*
    simple_uart_config(RTS_PIN_NUMBER, TX_PIN_NUMBER, CTS_PIN_NUMBER, RX_PIN_NUMBER, HWFC);
    
    NRF_UART0->INTENSET = UART_INTENSET_RXDRDY_Enabled << UART_INTENSET_RXDRDY_Pos;
    
    NVIC_SetPriority(UART0_IRQn, APP_IRQ_PRIORITY_LOW);
    NVIC_EnableIRQ(UART0_IRQn);
    //@snippet [UART Initialization] */
		
		const app_uart_comm_params_t comm_params =  
    {
        RX_PIN_NUMBER, 
        TX_PIN_NUMBER, 
        0, 
        0, 
        APP_UART_FLOW_CONTROL_DISABLED, 
        false, 
        UART_BAUDRATE_BAUDRATE_Baud57600
    }; 
        
    uint32_t err_code;
    APP_UART_FIFO_INIT(&comm_params, 
                       UART_RX_BUF_SIZE, 
                       UART_TX_BUF_SIZE, 
                       uart_error_handle, 
                       APP_IRQ_PRIORITY_LOW,
                       err_code);
    APP_ERROR_CHECK(err_code);
}



/**@brief   Function for handling UART interrupts.
 *
 * @details This function will receive a single character from the UART and append it to a string.
 *          The string will be be sent over BLE when the last character received was a 'new line'
 *          i.e '\n' (hex 0x0D) or if the string has reached a length of @ref NUS_MAX_DATA_LENGTH.
 
void UART0_IRQHandler(void)
{
    static uint8_t data_array[BLE_NUS_MAX_DATA_LEN];
    static uint8_t index = 0;
		static bool firstHeaderFound = false;
		static bool gotHeader = false;
		static bool gotBat = false;
		static bool gotHR = false;
		static bool gotStepMSB = false;
		static bool gotStepLSB = false;
		static bool gotCalMSB = false;
		static bool gotCalLSB = false;
		static uint8_t LB_HR_temp = 1;
		static uint8_t LB_Bat_temp = 90;
		static uint16_t LB_Steps_temp = 0;
		static uint16_t LB_Cal_temp = 0;
		
		
		
		
//    uint32_t err_code;

    //@snippet [Handling the data received over UART] 

    data_array[index] = simple_uart_get();
    index++;
		
		if (data_array[index] == NORDIC_PHYS_HEADER)
		{
			firstHeaderFound = true;
		}
		
		if (firstHeaderFound == false)
			return;

		if (data_array[index] == NORDIC_PHYS_HEADER)
		{
			gotHeader = true;
			index = 1;
		}
		
		if ((index == 2) && (gotHeader == true))
		{
			LB_HR_temp = data_array[index-1];
			gotHeader = false;
			gotHR = true;
		}
		
		if ((index == 3) && (gotHR == true))
		{
			LB_Bat_temp = data_array[index-1];
			gotHR = false;
			gotBat = true;
		}
		
		if ((index == 4) && (gotBat == true))
		{
			LB_Steps_temp = (data_array[index-1] << 8);;
			gotBat = false;
			gotStepMSB = true;
		}
		
		if ((index == 5) && (gotStepMSB == true))
		{
			LB_Steps_temp |= data_array[index-1];
			gotStepMSB = false;
			gotStepLSB = true;
		}
		
		if ((index == 6) && (gotStepLSB == true))
		{
			LB_Cal_temp = (data_array[index-1] << 8);
			gotStepLSB = false;
			gotCalMSB = true;
		}
		
		if ((index == 7) && (gotCalMSB == true))
		{
			LB_Cal_temp |= data_array[index-1];
			gotCalMSB = false;
			gotCalLSB = true;
		}
		
		if ((index == 8) && (gotCalLSB == true))
		{
			if (data_array[index-1] == NORDIC_PHYS_FOOTER)
			{
			gotCalLSB = false;
			LB_HR = LB_HR_temp;
			LB_Steps = LB_Steps_temp;
			LB_Cal = LB_Cal_temp;
			LB_Bat = LB_Bat_temp;
			index = 0;
			}
		}
		

    //@snippet [Handling the data received over UART]
}*/

void uartHandler (void)
{
//	static uint8_t uartData[4] = {0};
//	static uint8_t uartIndex = 0;
	static uint8_t uart_byte;
	static uint16_t mycounter = 0;
	
	static uint8_t data_array[BLE_NUS_MAX_DATA_LEN];
    static uint8_t index = 0;
		static bool firstHeaderFound = false;

		 uint16_t Flow = 0;
		 uint16_t CO2 = 0;
		 static uint16_t O2 = 0;
	uint16_t SensorNo = 0;

	static uint8_t send_less_for_debug = 0;
	
		if (app_uart_get(&uart_byte) == NRF_ERROR_NOT_FOUND)
		return;
	
	data_array[index++]=uart_byte;
	
	
	switch (index)
	{
		case 1:
			if (data_array[0]!=PKT_HEADER)
				index=0;
		break;
		case 8:
			index =0;
			if ((data_array[0]==PKT_HEADER)&&(data_array[7]==PKT_FOOTER))
			{

				Flow = (data_array[1]<<8) | data_array[2];
				O2   = (data_array[3]<<8) | data_array[4];
				//if (O2<2000) {O2++;} else {O2=1;}
				CO2  = (data_array[5]<<8) | data_array[6];
				sprintf((char*) data_array1,"%d,%d,%d",Flow,O2,CO2);
				ble_nus_send_string(&m_nus, data_array1, 20);
				
				
			}
			
			if ((data_array[0]==PKT_HEADER)&&(data_array[7]==PKT_FOOTER_FW_MSG))
			{
				Flow = (data_array[1]);//<<8) | data_array[2]; // date
				O2   = (data_array[2]);//<<8) | data_array[4]; //month
				//if (O2<2000) {O2++;} else {O2=2;}
				CO2  = (data_array[3]);//<<8) | data_array[6]; //year
				SensorNo = (data_array[4]);//<<8) | data_array[8]; //sensor_no
				sprintf((char*) data_array1,"FW%d,%d,%d,%d,",Flow,O2,CO2,SensorNo);
				ble_nus_send_string(&m_nus, data_array1, 20);
				
			}
			
			//clear the buffer
			data_array[0]=0;
			data_array[1]=0;
			data_array[2]=0;
			data_array[3]=0;
			data_array[4]=0;
			data_array[5]=0;
			data_array[6]=0;
			data_array[7]=0;
			data_array[8]=0;
			data_array[9]=0;
			
		break;
		default:
			break;
	}		
		
	
}


/**@brief Initialize the ANT HRM reception.
 */
//static void ant_hrm_rx_init(void)
//{
//    uint32_t err_code;
//    
//    err_code = sd_ant_network_address_set(ANTPLUS_NETWORK_NUMBER, m_ant_network_key);
//    APP_ERROR_CHECK(err_code);
//    
//    err_code = sd_ant_channel_assign(ANT_HRMRX_ANT_CHANNEL,
//                                     ANT_HRMRX_CHANNEL_TYPE,
//                                     ANTPLUS_NETWORK_NUMBER,
//                                     ANT_HRMRX_EXT_ASSIGN);
//    APP_ERROR_CHECK(err_code);

//    err_code = sd_ant_channel_id_set(ANT_HRMRX_ANT_CHANNEL,
//                                     ANT_HRMRX_DEVICE_NUMBER,
//                                     ANT_HRMRX_DEVICE_TYPE,
//                                     ANT_HRMRX_TRANS_TYPE);
//    APP_ERROR_CHECK(err_code);
//    
//    err_code = sd_ant_channel_radio_freq_set(ANT_HRMRX_ANT_CHANNEL, ANTPLUS_RF_FREQ);
//    APP_ERROR_CHECK(err_code);
//    
//    err_code = sd_ant_channel_period_set(ANT_HRMRX_ANT_CHANNEL, ANT_HRMRX_MSG_PERIOD);
//    APP_ERROR_CHECK(err_code);
//}

/**@brief Start receiving the ANT HRM data.
// */
//static void ant_hrm_rx_start(void)
//{
//    uint32_t err_code = sd_ant_channel_open(ANT_HRMRX_ANT_CHANNEL);
//    APP_ERROR_CHECK(err_code);
//}

/**@brief  Application main function.
 */
int main(void)
{
//	static unsigned int HRCnt = 30;
//	static unsigned int CalCnt = 10;
//	static unsigned int StepsCnt = 100;
	int n;
    // Initialize
    timers_init();
    uart_init();
    ble_ant_stack_init();
    gap_params_init();
    services_init();
    advertising_init();
    conn_params_init();
    sec_params_init();
	  nrf_gpio_pin_dir_set(RF_CON,NRF_GPIO_PIN_DIR_INPUT);
    
    // simple_uart_putstring((uint8_t*) START_STRING);
    // Initialize ANT HRM recieve channel.
  // ant_hrm_rx_init();Shimon
	
    advertising_start();
		
    // Initialize ANT HRM RX channel.
  //  ant_hrm_rx_start();SHimon
		
		//app_uart_put((uint8_t) 0xAA);
		
    
    // Enter main loop
    for (;;)
    {
      //  power_manage();
			//n = sprintf((char*) data_array1,"%s %d %s %d %s %d", "HR:", HRCnt, " Cal:", CalCnt, " Step:", StepsCnt);
			//n = sprintf((char*) data_array1,"%s %d ","HR:", LB_HR);
			//ble_nus_send_string(&m_nus, data_array1, 20);
			//n = sprintf((char*) data_array1,"%s %d ","Cal:", LB_Cal);
			//ble_nus_send_string(&m_nus, data_array1, 20);
//			n = sprintf((char*) data_array1,"%s %d ","ANT RX:", computed_heart_rate);
//			ble_nus_send_string(&m_nus, data_array1, 20);
			
			//uartHandler();
			//app_uart_put((uint8_t) 0xAA);
			heart_rate_meas_send();
			
			//pin_rf_con_state = nrf_gpio_pin_read(RF_CON);
			//if (pin_rf_con_state == 0) {
			//	NVIC_SystemReset();
			//}
			
			//app_uart_put((uint8_t)(computed_heart_rate+1)/*p_data[0]*/); //send info to arduino to process order
			//nrf_delay_us(1000000);
//			HRCnt++;
//			CalCnt++;
//			StepsCnt++;
//			if (HRCnt==131) {
//				HRCnt = 30;
//				CalCnt = 10;
//				StepsCnt = 100;
//			}
    }
}



/** 
 * @}
 */
